#include "HEPUtils/Vectors.h"
#include "TestTools.h"


double pseudorapidity(HEPUtils::P4 vec) {
  return std::copysign(log((vec.p() + fabs(vec.pz())) / vec.pT()), vec.pz());
}


int main() {

  double lower_bound=-100000, upper_bound=100000;
  std::uniform_real_distribution<double> unif(lower_bound,upper_bound);
  std::default_random_engine generator(time(0));

  double px, py, pz, m;
  HEPUtils::P4 vec;
  double etaTest;
  double eta;

  for (int i=0; i<1000; i++) {
    px = unif(generator);
    py = unif(generator);
    pz = unif(generator);
    m = unif(generator);

    if (m < 0) {
      i--;
      continue;
    }

    vec.setPM(px, py, pz, m);

    if ( !almost_equal(vec.eta(), pseudorapidity(vec), 0.1)) {
      return 1;
    }
  }
  return 0;
}
